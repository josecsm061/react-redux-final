export const ADD_ITEM_TO_CART = "ADD_ITEM_TO_CART";
export const FETCH_BIKES_SECCUESS = "FETCH_BIKES_SECCUESS";

export const addItemToCart = (item) => {
    return {
        type: ADD_ITEM_TO_CART,
        payload: item
    }
}

export const fetchBikes = (dispatch) => {
    return (dispatch) => {
        fetch(`https://specializedbikes.herokuapp.com/api/v1/bikes`)
            .then(response => response.json())
            .then(({ data }) => dispatch({ type: FETCH_BIKES_SECCUESS, payload: data }))
    }
}