import { createStore, applyMiddleware } from 'redux';
import { ADD_ITEM_TO_CART, FETCH_BIKES_SECCUESS } from './actions';
import thunk from 'redux-thunk';


const initialState = {
    cart: [],
    bikes: []
}

function reducer(state = initialState, action) {
    switch (action.type) {
        case ADD_ITEM_TO_CART:
            return {
                ...state,
                cart: [...state.cart, action.payload]
            };
        case FETCH_BIKES_SECCUESS:
            return {
                ...state,
                bikes: action.payload
            }
    }
    return state;
}

export default createStore(reducer, applyMiddleware(thunk));