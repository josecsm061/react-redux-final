import n from 'numeral';

export default function FormattedPrice({ price }) {
  return `$${ n(price).format('0,0.00') }`;
}
