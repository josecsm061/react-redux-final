import React, { Component } from 'react';

import Bike from './Bike';
import AddItem from './AddItem';
import { connect } from 'react-redux';
import { fetchBikes } from '../../actions'
import './Bikes.css';

class Bikes extends Component {
  componentDidMount() {
    this.props.loadBikes();
  }

  render() {
    return (
      <div style={{ width: '80%' }}>
        <ul className='gallery'>

          {this.props.bikes.map(bike => {
            return (
              <li key={bike.id} className='gallery__item bike'>
                <AddItem bike={bike} />
                <Bike {...bike} />
              </li>
            );
          })}
        </ul>
        <p className='pagination'>View All <i className='fa fa-long-arrow-right' /></p>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    bikes: state.bikes
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    loadBikes() {
      dispatch(fetchBikes())
    }
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Bikes);