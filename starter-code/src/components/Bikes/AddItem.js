import React from 'react';
import { addItemToCart } from "../../actions";
import { connect } from 'react-redux';

const AddItem = (props) => {
  return (
    <button
      className='bike__add' onClick={() => props.addBikeToCart(props.bike)}>
      <i className='fa fa-plus' />Add
    </button>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    addBikeToCart(bike) {
      dispatch(addItemToCart(bike))
    }
  }
}

export default connect(null, mapDispatchToProps)(AddItem);
