import React from 'react';

import './Header.css';

export default function Header() {
  return (
    <header style={{ backgroundColor: 'white' }}>
      <div className='container grid grid--spaced grid--center header'>
        <a href='https://www.specialized.com'>
          <img
            className='header__logo'
            src='https://www.fraserbicycle.com/merchant/3091/images/site/specialized-logo.png'
            alt='' />
        </a>

        <ul className='menu grid'>
          <li className='menu__item'>Bikes</li>
          <li className='menu__item'>Equipment</li>
          <li className='menu__item'>Inside Specialized</li>
        </ul>

        <a href='/' className='search'>
          <i className='fa fa-search' />
        </a>
      </div>
    </header>
  );
}
