import React, { Component } from 'react';

import FormatPrice from '../Utils/FormatPrice';
import { connect } from 'react-redux';

import './ShoppingCart.css';

const ShoppingCart = (props) => {
  return (
    <ul className='cart'>
      {props.cart.map(bike => {
        return (
          <li key={bike.id} className='item'>
            <h3 className='item__title'>{bike.name}</h3>
            <p className='item__sku'>{bike.sku}</p>
            <p className='item__color'>{bike.colorName}</p>
            <p className='item__size'>{bike.size}</p>
            <p className='item__price'>PMSM <FormatPrice price={bike.price} /></p>
            <button
              className='item__remove'>
              <i className='fa fa-trash' />
            </button>
          </li>
        );
      })}
      <li className='total'>
        <span className='total__label'>Total</span>
        <span className='total__price'>
          <FormatPrice price={props.cart.map(bike=>bike.price).reduce((a,b) => a + b, 0)} />
        </span>
      </li>
    </ul>
  );

}

const mapStateToProps = ({ cart }) => ({ cart });

export default connect(mapStateToProps, null)(ShoppingCart);
